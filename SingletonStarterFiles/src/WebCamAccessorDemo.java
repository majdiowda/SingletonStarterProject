public class WebCamAccessorDemo {
	
	public static void main(String args[]) {
		//	WebCamAccessor1 obj = new WebCamAccessor1();
                //Compilation error not allowed
		WebCamAccessor obj1 = 
			WebCamAccessor.getSingletonCam(); 
		WebCamAccessor obj2 = 
			WebCamAccessor.getSingletonCam();
	
		// Your Business Logic
		System.out.println("Singleton object obtained");
	}
}

