class WebCamAccessor { 

	private static WebCamAccessor singletonCam;
	// private Constructor prevents any class from instantiating. 

	private WebCamAccessor() {
		//	 Optional Code
	}
	public static synchronized WebCamAccessor getSingletonCam() {
	// public static synchronized 
		if (singletonCam == null) {
			singletonCam = new WebCamAccessor();
			System.out.println("Camera Accessor Created");
		}
		else
		{
		System.out.println("Another user has the web cam");
		}
		return singletonCam;
	}
	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}
}
